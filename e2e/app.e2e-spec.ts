import { JqueryReactivexPage } from './app.po';

describe('jquery-reactivex App', () => {
  let page: JqueryReactivexPage;

  beforeEach(() => {
    page = new JqueryReactivexPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
