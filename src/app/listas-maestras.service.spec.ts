import { TestBed, inject } from '@angular/core/testing';

import { ListasMaestrasService } from './listas-maestras.service';

describe('ListasMaestrasService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ListasMaestrasService]
    });
  });

  it('should be created', inject([ListasMaestrasService], (service: ListasMaestrasService) => {
    expect(service).toBeTruthy();
  }));
});
