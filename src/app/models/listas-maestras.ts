export interface Municipio {
    codigoMunicipio: number;
    municipio: string;
}

export interface Departamento {
    codigo: number;
    descripcion: string;
    municipios: Municipio[];
}

export interface Paises {
    cod: number;
    descripcion: string;
    departamentos: Departamento[];
}

