import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Paises } from 'app/models/listas-maestras';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/publish';

@Injectable()
export class ListasMaestrasService {

  private paisesUrl = 'assets/api/listas/paises.json';

  constructor(private http: HttpClient) { }

  // Consulta todos los paises de la lista maestra
  getPaises(): Observable<Paises[]> {
    return this.http.get<Paises[]>(this.paisesUrl)
      .do(data => console.log('All: ' + JSON.stringify(data)))
      .catch(this.handleError);
  }

  // Consulta por texto los paises de la lista maestra
  getPaisesQuery(query: string): Observable<Paises[]> {
    return this.http.get<Paises[]>(this.paisesUrl)
      //.do(data => console.log('All: ' + JSON.stringify(data)))
      .map((paises: Paises[]) => paises.filter(p => p.descripcion.toLowerCase().includes(query.toLowerCase(), 0)))
      .catch(this.handleError);
  }

  private handleError(err: HttpErrorResponse) {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let errorMessage = '';
    if (err.error instanceof Error) {
      // A client-side or network error occurred. Handle it accordingly.
      errorMessage = `An error occurred: ${err.error.message}`;
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      errorMessage = `Server returned code: ${err.status}, error message is: ${err.message}`;
    }
    console.error(errorMessage);
    return Observable.throw(errorMessage);
  }

}
