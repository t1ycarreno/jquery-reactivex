export default class Validations { 

    static validatePrefixes(text: string, prefijos: string[]) {
        for (let i in prefijos) {
            if (text.includes(prefijos[i], 0)) {
                return true;
            }
        }
        return false;
    }
}