import Regex from "app/utils/regex";
import Validations from "app/utils/validations";
import { Observable } from "rxjs/Observable";

declare var $: any;

export default class Utils {

    /**
     * Crea un Observable para un componente de tipo InputText con una longitud específica.
     * La primera operación "map" captura el valor del InputText.
     * La segunda operación "map" valida longitud del texto ingresado.
     * El observable retorna un boolean. 
     * @param idInputText
     * @param minimumLeght 
     */
    static obsInputTextCelular(idInputText: string, minimumLeght: number, prefijos: string[]) {
        return $(idInputText).keyupAsObservable()
            //Toma el valor ingresado en inputText
            .map(function (ev) {
                return $(ev.target).val();
            })
            //Valida que la longitud sea exactamente la enviada como argumento
            //Valida que los 3 primeros números sean prefijos aceptados
            //Valida que el 4 dígito no sea 0 ni 1
            //Valida que los 7 últimos dígitos no esten repetidos cosecutivamente
            .map(function (text) {
                return text.length === minimumLeght && Validations.validatePrefixes(text, prefijos) &&
                    text.substring(3, 4) != 0 && text.substring(3, 4) != 1 &&
                    Regex.validateSameConsecutives(text.substring(3));
            })
            .distinctUntilChanged();
    }

    /**
     * Implementación la lista predictiva (autocomplete) de paises
     * @param idInputText 
     */
    static obsInputTextListaPaises(idInputText: string) {
        return $(idInputText).keyupAsObservable()
            //Toma el valor ingresado en inputText
            .map(function (ev) {
                return $(ev.target).val();
            })
            .filter(text => text != '' && text.length > 1)
            .throttle(100)
            .debounce(750)
            .distinctUntilChanged();
    }

    /**
     * Crea un Observable para un componente de tipo InputText con longitud máxima y mínima.
     * La primera operación "map" captura el valor del InputText.
     * La segunda operación "map" valida las longitud del texto ingresado.
     * El observable retorna un boolean. 
     * @param idInputText 
     * @param minimumLeght 
     * @param maximeLenght 
     */
    static obsInputTextMinMax(idInputText: string, minimumLeght: number, maximeLenght: number) {
        return $(idInputText).keyupAsObservable()
            .map(function (ev) {
                return $(ev.target).val();
            })
            .map(function (text) {
                return text.length > minimumLeght && text.length < maximeLenght;
            })
            .throttle(500)
            .distinctUntilChanged();
    }

    /**
     * Crea un Observable para un componente de tipo InputText con longitud mínima y una
     * expresión regex que valida el formato de email.
     * El observable retorna un boolean.
     * @param idInputText 
     * @param minimumLeght
     */
    static obsInputTexEmail(idInputText: string, minimumLeght: number) {
        return $(idInputText).keyupAsObservable()
            .map(function (ev) {
                return $(ev.target).val();
            })
            //.filter(t => t.length > 10)
            .map(function (text) {
                return text.length > minimumLeght && Regex.validateEmail(text);
            })
            .throttle(500)
            .distinctUntilChanged();
    }

}