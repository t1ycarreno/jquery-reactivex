export default class Regex {

    //Valida el formato de los campos tipo email
    static validateEmail(email: string) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    //Valida que se numérico de 0 - 9
    static validateNumérico(text: string) {
        var re = /^[0-9]{1,10}$/;
        return re.test(text);
    }

    //Valida que si tiene 7 consecutivos repetidos
    static validateSameConsecutives(text: string) {
        var re = /^(?!(.)\1{6})/;
        return re.test(text);
    }
}