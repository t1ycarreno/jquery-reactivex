import { Component, OnInit, OnDestroy } from '@angular/core';
import Utils from "app/utils/rx-forms";
import { ListasMaestrasService } from 'app/listas-maestras.service';
import 'rxjs/add/operator/switchMap';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit, OnDestroy {

  title = 'Formulario con Rx';
  subscription: any;
  subscriptionPaises: any;
  subscriptionTwo: any;
  subscriptionThree: any;
  subscriptionFour: any;
  subscriptionFive: any;

  constructor(private listasMaestrasService: ListasMaestrasService) {
  }

  ngOnInit(): void {

    //Crea la subscripción para el InputText de Celular
    this.subscription =
      this.subscribeInputText(Utils.obsInputTextCelular('#textInputPhone', 10, ['300', '301', '303', '304', '305', '310'])
        , '#address-field', '#warning-phone');

    //Consulto las listas maestras
    this.subscriptionPaises =
      Utils.obsInputTextListaPaises('#textInputPaisNacimiento')
        .flatMap(query => this.listasMaestrasService.getPaisesQuery(query))
        .map(onlyfive => onlyfive.slice(0, 3))
        .subscribe(resultsWithFilters => {
          //Tratar la vistas
          console.log(resultsWithFilters);
        },
        error => console.error(error));

    //Crea la subscripción para el InputText de Dirección de residencia
    this.subscriptionTwo =
      this.subscribeInputText(Utils.obsInputTextMinMax('#textInputAddress', 2, 20)
        , '#floor-field', '#warning-address');

    //Crea la subscripción para el InputText de Piso/Oficina/Apto
    this.subscriptionThree =
      this.subscribeInputText(Utils.obsInputTextMinMax('#textInputFloor', 2, 20)
        , '#city-field', '#warning-floor');

    //Crea la subscripción para el InputText de Ciudad
    this.subscriptionFour =
      this.subscribeInputText(Utils.obsInputTextMinMax('#textInputCity', 2, 8)
        , '#email-field', '#warning-city');

    //Crea la subscripción para el InputText de Email (este usa también regex)
    this.subscriptionFour =
      this.subscribeInputText(Utils.obsInputTexEmail('#textInputEmail', 2)
        , '#fuente-ingresos-field', '#warning-email');

    //Crea la subscripción para el InputText de Fuente Ingresos
    this.subscriptionFive =
      this.subscribeInputText(Utils.obsInputTextMinMax('#textInputFuente', 2, 8)
        , '#ingresos-field', '#warning-fuente');
  }

  subscribeInputText(obs: any, idDependentComponent: string, idMessageValidate: string) {
    obs.subscribe(
      function (data) {
        console.log(data);
        $(idMessageValidate).attr("hidden", data);
        $(idDependentComponent).attr("hidden", !data);
        // Salvar el dato del campo en el objeto temporal
      },
      function (e) {
        console.log(e);
      }
    );
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.clear();
    }
    if (this.subscriptionPaises) {
      this.subscriptionPaises.clear();
    }
    if (this.subscriptionTwo) {
      this.subscriptionTwo.clear();
    }
    if (this.subscriptionThree) {
      this.subscriptionThree.clear();
    }
    if (this.subscriptionFour) {
      this.subscriptionFour.clear();
    }
    if (this.subscriptionFive) {
      this.subscriptionFive.clear();
    }
  }
}
